/*
 *  Advanced Cache simulation project
 *  Class UCR IE-521
 *  Semester: II-2019
 *  Jorge Munoz Taylor
 *  A53863
*/

#include <gtest/gtest.h>
#include <time.h>
#include <stdlib.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <Victimcache.h>

#define KB 1024
#define KB4 4096
#define MIN_ASSOCIATIVITY 1
#define MAX_ASSOCIATIVITY 4
#define L2_MULT 4
#define L2_ASSO_MULT 2
#define VC_SIZE 16
#define INIT 0

using namespace std;

class VCcache : public ::testing::Test{
	protected:
		int debug_on;
		virtual void SetUp()
		{
  		/* Parse for debug env variable */
  		get_env_var("TEST_DEBUG", &debug_on);
		};
};

/*
 * TEST1: L1 miss VC hit
 * 1. Fill the VC with the same tag
 * 2. The L1 cache is empty
 * 3. Select a ramdom idx and associativity
 * 4. Search an specific tag
 */
TEST_F(VCcache,l1_miss_vc_hit){
	int status = OK;
	int idx;
	int tag;
	int associativity;
	
	struct l1_vc_entry_info l1_vc_info;
  	struct operation_result l1_result = {};
  	struct operation_result vc_result = {};										

	/* Select a random idx and associativity, and a specific tag */
	idx           = rand()%KB;
	tag           = KB4;
	associativity = MIN_ASSOCIATIVITY << (rand()% MAX_ASSOCIATIVITY);

	struct entry l1_cache_blocks[ KB*associativity ];
	struct entry vc_cache_blocks[ VC_SIZE ];

	/* Check for a L1 miss and VC hit */
	DEBUG(debug_on, l1_miss_vc_hit_test);

	l1_vc_info.l1_tag           = tag;
	l1_vc_info.l1_idx           = idx;
	l1_vc_info.l1_associativity = associativity;
	l1_vc_info.vc_associativity = VC_SIZE;


	/* Fill the VC with an specific value */
	for ( int i =  0; i < VC_SIZE; i++)
	{
		vc_cache_blocks [ i ].valid = true;
		vc_cache_blocks [ i ].tag   = KB4;
		vc_cache_blocks [ i ].dirty = false;

	}/* For end */


	/* L1_VC algorithm */
	status = lru_replacement_policy_l1_vc(&l1_vc_info,
      	                        	      true,
        	                      	      l1_cache_blocks,
          	                    	      vc_cache_blocks,
            	                  	      &l1_result,
              	                	      &vc_result);

	EXPECT_EQ( status, OK );

	EXPECT_EQ( l1_result.miss_hit, MISS_STORE );
	EXPECT_EQ( vc_result.miss_hit, HIT_LOAD );

}/* TEST 1 end */




/*
 * TEST2: L1 miss VC miss
 * 1. Fill the VC with a tag out of range
 * 2. The L1 cache is empty
 * 3. Select a random idx, tag and associativity
 * 4. Search the tag in the cache
 */
TEST_F(VCcache,l1_miss_vc_miss){
	int status = OK;
	int idx;
	int tag;
	int associativity;
	
	struct l1_vc_entry_info l1_vc_info;
  	struct operation_result l1_result = {};
  	struct operation_result vc_result = {};										

	/* Select a random idx, tag and associativity */
	idx           = rand()%KB;
	tag           = rand()%KB4;
	associativity = MIN_ASSOCIATIVITY << (rand()% MAX_ASSOCIATIVITY);

	struct entry l1_cache_blocks[ KB*associativity ];
	struct entry vc_cache_blocks[ VC_SIZE ];

	/* Check for a L1 miss and VC miss */
	DEBUG(debug_on, l1_miss_vc_miss_test);

	l1_vc_info.l1_tag           = tag;
	l1_vc_info.l1_idx           = idx;
	l1_vc_info.l1_associativity = associativity;
	l1_vc_info.vc_associativity = VC_SIZE;

	/* Fill the VC cache line with a specific value */
	for ( int i =  0; i < VC_SIZE; i++)
	{
		vc_cache_blocks [ i ].valid = true;
		vc_cache_blocks [ i ].tag   = KB4 + 1;
		vc_cache_blocks [ i ].dirty = false;

	}/* For end */


	/* L1_VC algorithm */
	status = lru_replacement_policy_l1_vc(&l1_vc_info,
      	                        	      true,
        	                      	      l1_cache_blocks,
          	                    	      vc_cache_blocks,
            	                  	      &l1_result,
              	                	      &vc_result);

	EXPECT_EQ( status, OK );

	EXPECT_EQ( l1_result.miss_hit, MISS_STORE );
	EXPECT_EQ( vc_result.miss_hit, MISS_LOAD );

}/* TEST 2 end */