/*
 *  Advanced Cache simulation project
 *  Class UCR IE-521
 *  Semester: II-2019
 *  Jorge Munoz Taylor
 *  A53863
*/

#include <gtest/gtest.h>
#include <time.h>
#include <stdlib.h>
#include <debug_utilities.h>
#include <L1cache.h>

#define KB 1024
#define RRPV_MAX    3
#define RRPV_MIN    0
#define RRPV_ACTUAL 2
#define INIT 0

#define MAX_ASSOCIATIVITY 4

using namespace std;

class L1cache : public ::testing::Test{
	protected:
		int debug;
		virtual void SetUp()
		{
  		/* Parse for debug env variable */
  		get_env_var("TEST_DEBUG", &debug);
		};
};


/*
 * TEST1: Verifies miss and hit scenarios for srrip policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST(L1cache, hit_miss_srrip)
{
  int    status;
  int    i;
  int    idx;
  int    tag;
  int    associativity;
  enum   miss_hit_status expected_miss_hit;
  bool   loadstore                 = STORE;
  bool   debug                     = INIT;
  struct operation_result result   = {};

  /* Fill a random cache entry */
  idx = rand()%KB;
  tag = rand()%4096;
  associativity = 1 << (rand() % MAX_ASSOCIATIVITY);

  if (debug) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
 
  struct entry cache_line[ KB*associativity ];
  /* Check for a miss */
  DEBUG(debug, Checking miss operation);

  for (i = 0 ; i < 2; i++){
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[idx*associativity+i].valid    = true;
      cache_line[idx*associativity+i].tag      = rand()%4096;
      cache_line[idx*associativity+i].dirty    = false;
      cache_line[idx*associativity+i].rp_value = (associativity <= 2)? rand()%associativity: RRPV_MAX;
      
      while (cache_line[idx*associativity+i].tag == tag) {
        cache_line[idx*associativity+i].tag = rand()%4096;
      }
    }

    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    status = srrip_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug));
    EXPECT_EQ(status, OK);
    EXPECT_EQ(result.dirty_eviction, false);
    expected_miss_hit = (loadstore==STORE) ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same 
   * tag now we will get a hit
   */
  DEBUG(debug, Checking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = srrip_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug);
    EXPECT_EQ(status, OK);
    EXPECT_EQ(result.dirty_eviction, false);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }

}/* test 1 end */



/*
 * TEST2: Verifies miss and hit scenarios for lru policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST(L1cache, hit_miss_lru) 
{
  int    status;
  int    i; 
  int    idx;
  int    tag;
  int    associativity;
  enum   miss_hit_status expected_miss_hit;
  bool   loadstore                 = STORE;
  bool   debug                     = 0;
  struct operation_result result   = {};
  bool   expected_eviction;


  /* Fill a random cache entry */
  idx = INIT;
  associativity = 1 << (rand() % MAX_ASSOCIATIVITY);  
 
  struct entry cache_line[associativity];

  /* Fill cache line */
  for ( i = 0; i < associativity; i++) 
  {
    cache_line[i].valid = true;
    cache_line[i].tag   = rand()%4090;
    cache_line[i].dirty = false;
    
  }/* for end */

  /* Check for a miss */
  DEBUG(debug, Checking miss operation);

  tag = 4094;
  loadstore = LOAD;
  
  for (i = 0 ; i < 2; i++)
  {    
    status = lru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug));

    tag++;

    expected_eviction = false;
    EXPECT_EQ(status, OK);
    EXPECT_EQ(result.dirty_eviction, expected_eviction );
    expected_miss_hit = (loadstore==LOAD) ? MISS_LOAD: MISS_STORE;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
    
  }/* for end */


  /*
   * Check for hit: block was replaced in last iteration, if we used the same 
   * tag now we will get a hit
   */
  DEBUG(debug,Checking hit operation);

  tag = 4095;

  for (i = 0 ; i < 2; i++)
  {
    loadstore = (bool)i;
    status = lru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug);

    EXPECT_EQ( status, OK);
    EXPECT_EQ( result.dirty_eviction, false );
    expected_miss_hit = (loadstore==LOAD) ? HIT_LOAD:HIT_STORE;
    EXPECT_EQ( result.miss_hit, expected_miss_hit);
  } 

}/* test 2 end */



/*
 * TEST3: Verifies miss and hit scenarios for nru policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST(L1cache, hit_miss_nru) 
{

  int    status;
  int    i; 
  int    idx;
  int    tag;
  int    associativity;
  enum   miss_hit_status expected_miss_hit;
  bool   loadstore                 = STORE;
  bool   debug                     = 0;
  struct operation_result result   = {};
  bool   expected_eviction;


  /* Fill a random cache entry */
  idx = 0;
  tag = rand()%4096;
  associativity = 1 << (rand() % MAX_ASSOCIATIVITY);
   
  struct entry cache_line[associativity];

  /* Fill cache line */
  for ( i = 0; i < associativity; i++) 
  {
    cache_line[i].valid    = true;
    cache_line[i].tag      = rand()%4090;
    cache_line[i].dirty    = false;
    cache_line[i].rp_value = RRPV_ACTUAL - 1;
  }/* for end */

  /* Check for a miss */
  DEBUG(debug,Checking miss operation);

  tag = 4094;
  int there_is_a_store = false;

  for (i = 0 ; i < 2; i++)
  {
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    
    status = nru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug));

    EXPECT_EQ (status, OK);
    tag++;

    if( there_is_a_store == true ) expected_eviction = true;
    else expected_eviction = false;

    EXPECT_EQ(result.dirty_eviction, expected_eviction );

    expected_miss_hit = (loadstore == LOAD) ? MISS_LOAD: MISS_STORE;
    EXPECT_EQ( result.miss_hit, expected_miss_hit);   

    if(loadstore == STORE) there_is_a_store = true;
  }


  /*
   * Check for hit: block was replaced in last iteration, if we used the same 
   * tag now we will get a hit
   */  
  DEBUG(debug,Checking hit operation);

  tag = 4095;

  for (i = 0 ; i < 2; i++)
  {
    loadstore = (bool)i;
    status = nru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug);

    EXPECT_EQ( status, OK);
    EXPECT_EQ( result.dirty_eviction, false );

    expected_miss_hit = (loadstore == LOAD) ? HIT_LOAD:HIT_STORE;
    EXPECT_EQ( result.miss_hit, expected_miss_hit);
  } 

}/* test 3 end */



/*
 * TEST4: Verifies replacement policy promotion and eviction
 * 1. Choose a random policy 
 * 2. Choose a random associativity
 * 3. Fill a cache entry
 * 4. Insert a new block A
 * 5. Force a hit on A
 * 6. Check rp_value of block A
 * 7. Keep inserting new blocks until A is evicted
 * 8. Check eviction of block A happen after N new blocks were inserted
 * (where N depends of the number of ways)
 */
TEST(L1cache, promotion){
  int    status;
  int    i; 
  int    policy;
  int    idx;
  int    tag;
  int    associativity;
  enum   miss_hit_status expected_miss_hit;
  bool   loadstore                 = STORE;
  bool   debug                     = 0;
  struct operation_result result   = {};
  bool   expected_eviction;


  /* Fill a random cache entry */
  policy        = rand()%2;
  idx           = INIT;
  associativity = 1 << (rand()% MAX_ASSOCIATIVITY);
   
  struct entry cache_line[associativity];
  tag = 300;
  

  switch( policy )
  {
    /* LRU */
    case LRU:

      /* Fill cache line */
      for ( i =  0; i < associativity; i++) 
      {
        cache_line[i].valid    = true;
        cache_line[i].tag      = tag;
        cache_line[i].dirty    = false;
        cache_line[i].rp_value = INIT;
      }/* for end */

      /* Force a Hit */
      for( i=0; i<2; i++)
      {
        status = lru_replacement_policy(idx, 
                              4000, 
                              associativity,
                              loadstore,
                              cache_line,
                              &result,
                              bool(debug));
      }/* for end */

      EXPECT_EQ(status, OK);

      /* Check if there is a HIT */
      expected_miss_hit = HIT_STORE;
      EXPECT_EQ( result.miss_hit, expected_miss_hit);

      /* check rp value in inserted block */
      if( associativity > 1)
      {
        for( i=0; i<associativity; i++ )
        {
          if(cache_line[i].tag == 4000)
          {
            status = cache_line[i].rp_value;
            i = associativity;
          } 
        }/* for end */
      }/* if end */
      else
      {
        status = cache_line[idx].rp_value;
      }/* else end */

      /* Check if rp value is 0 */
      EXPECT_EQ( status, INIT );


      /* Insert new cache block until get an eviction in */
      /* the first inserted block                        */    
      for( i=0; i<associativity; i++)
      {
        tag++;
        lru_replacement_policy(idx, 
                              tag, 
                              associativity,
                              loadstore,
                              cache_line,
                              &result,
                              bool(debug));
      }/* for end */

      EXPECT_EQ( result.evicted_address, INIT );

      break;



    /* NRU */
    case NRU:

      /* Fill cache line */
      for ( i =  0; i < associativity; i++) 
      {
        cache_line[i].valid    = true;
        cache_line[i].tag      = tag;
        cache_line[i].dirty    = false;
        cache_line[i].rp_value = RRPV_ACTUAL - 1;
      }/* for end */

      /* Force a Hit */
      for( i=0; i<2; i++)
      {
        status = nru_replacement_policy(idx, 
                              4000, 
                              associativity,
                              loadstore,
                              cache_line,
                              &result,
                              bool(debug));
      }/* for end */

      EXPECT_EQ(status, OK);

      /* Check if there is a HIT */
      expected_miss_hit = HIT_STORE;
      EXPECT_EQ( result.miss_hit, expected_miss_hit);

      /* check rp value in inserted block */
      if( associativity > 1)
      {
        for( i=0; i<associativity; i++ )
        {
          if(cache_line[i].tag == 4000)
          {
            status = cache_line[i].rp_value;
            i = associativity;
          } 
        }/* for end */
      }/* if end */
      else
      {
        status = cache_line[idx].rp_value;
      }/* else end */

      /* Check if rp value is 0 */
      EXPECT_EQ( status, INIT );


      /* Insert new cache block until get an eviction in */
      /* the first inserted block                        */    
      for( i=0; i<associativity; i++)
      {
        tag++;
        nru_replacement_policy(idx, 
                              tag, 
                              associativity,
                              loadstore,
                              cache_line,
                              &result,
                              bool(debug));

        if( result.dirty_eviction == true ) i = associativity; 
      }/* for end */

      EXPECT_EQ( result.evicted_address, INIT );

      break;



    /* SRRIP */
    case RRIP:

      /* Fill cache line */
      for ( i = 0; i < associativity; i++) 
      {
        cache_line[i].valid    = 1;
        cache_line[i].tag      = tag;
        cache_line[i].dirty    = false;
        cache_line[i].rp_value = RRPV_MAX;
      }/* for end */

      /* Force a Hit */
      for( i=0; i<2; i++)
      {
        status = srrip_replacement_policy(idx, 
                              4000, 
                              associativity,
                              loadstore,
                              cache_line,
                              &result,
                              bool(debug));
      }/* for end */

      EXPECT_EQ(status, OK);

      /* Check if there is a HIT */
      expected_miss_hit = HIT_STORE;
      EXPECT_EQ( result.miss_hit, expected_miss_hit);

      /* check rp value in inserted block */
      if( associativity > 1)
      {
        for( i=0; i<associativity; i++ )
        {
          if(cache_line[i].tag == 4000)
          {
            status = cache_line[i].rp_value;
            i = associativity;
          } 
        }/* for end */
      }/* if end */
      else
      {
        status = cache_line[idx].rp_value;
      }/* else end */

      /* Check if rp value is 0 */
      EXPECT_EQ( status, INIT );


      /* Insert new cache block until get an eviction in */
      /* the first inserted block                        */    
      for( i=0; i<associativity; i++)
      {
        tag++;
        srrip_replacement_policy(idx, 
                              tag, 
                              associativity,
                              loadstore,
                              cache_line,
                              &result,
                              bool(debug));

        if( result.dirty_eviction == true ) i = associativity; 
      }/* for end */

      EXPECT_EQ( result.evicted_address, 4000 );
      break;

    default:
      return;
      break;
  }/* switch end */
 
}/* TEST4 end*/



/*
 * TEST5: Verifies evicted lines have the dirty bit set accordantly to the operations
 * performed.
 * 1. Choose a random policy
 * 2. Choose a random associativity
 * 3. Fill a cache entry with only read operations
 * 4. Force a write hit for a random block A
 * 5. Force a read hit for a random block B
 * 6. Force read hit for random block A
 * 7. Insert lines until B is evicted
 * 8. Check dirty_bit for block B is false
 * 9. Insert lines until A is evicted
 * 10. Check dirty bit for block A is true
 */
TEST(L1cache, writeback)
{
  int    status;
  int    idx;
  int    tag;
  int    associativity             = 1 << (rand()%4);
  bool   loadstore                 = LOAD;
  bool   debug                     = 0;
  struct operation_result result   = {};
  int    policy;
  int    expected_eviction;

  struct entry cache_line_A[associativity];
  struct entry cache_line_B[associativity];

  /* Random policy */
  policy = rand()%2;
  idx    = INIT;

  /* Fill cache line */
  for ( int i = 0; i < associativity; i++) 
  {
    cache_line_A[i].valid    = true;
    cache_line_A[i].tag      = 300;
    cache_line_A[i].dirty    = false;
    cache_line_A[i].rp_value = (policy==RRIP)? RRPV_MAX: 1;

    cache_line_B[i].valid    = true;
    cache_line_B[i].tag      = 300;
    cache_line_B[i].dirty    = false;
    cache_line_B[i].rp_value = (policy==RRIP)? RRPV_MAX: 1;
  }/* for end */

  DEBUG(debug,Checking dirty bit values);

  switch(policy)
  {
    /* LRU */
    case 0:
      for( int i=0; i<2; i++ ){
        status = lru_replacement_policy(idx, 
                                     4000, 
                                     associativity,
                                     STORE,
                                     cache_line_A,
                                     &result,
                                     bool(debug));
      }/* for end */

      EXPECT_EQ(status, OK);
      /* write hit */
      EXPECT_EQ(result.miss_hit, HIT_STORE);


      for( int i=0; i<2; i++ ){    
      status = lru_replacement_policy(idx, 
                                     300, 
                                     associativity,
                                     LOAD,
                                     cache_line_A,
                                     &result,
                                     bool(debug));
      }/* for end */

      EXPECT_EQ(status, OK);
      /* read hit */
      EXPECT_EQ(result.miss_hit, HIT_LOAD);
      

      for( int i=0; i<2; i++ ){
        status = lru_replacement_policy(idx, 
                                     4000, 
                                     associativity,
                                     LOAD,
                                     cache_line_B,
                                     &result,
                                     bool(debug));
      }/* for end */

      EXPECT_EQ(status, OK);
      /* read hit */
      EXPECT_EQ(result.miss_hit, HIT_LOAD);


      for( int i = 0; i<associativity; i++)
      {
        tag++;
        status = lru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     LOAD,
                                     cache_line_B,
                                     &result,
                                     bool(debug));
      }/* for end */

      EXPECT_EQ(status, OK);
      EXPECT_EQ( cache_line_B[idx].dirty, false);


      tag = 300;
     
      /* Insert new cache block until get an eviction in */
      /* the first inserted block                        */    
      for( int i=0; i<associativity; i++)
      {
        tag++;
        lru_replacement_policy(idx, 
                              tag, 
                              associativity,
                              LOAD,
                              cache_line_A,
                              &result,
                              bool(debug));
      }/* for end */

      EXPECT_EQ( result.evicted_address, INIT );

      break;
    
    
    /* NRU */
    case 1:
      for( int i=0; i<2; i++ ){
        status = nru_replacement_policy(idx, 
                                     4000, 
                                     associativity,
                                     STORE,
                                     cache_line_A,
                                     &result,
                                     bool(debug));
      }/* for end */

      EXPECT_EQ(status, OK);
      /* write hit */
      EXPECT_EQ(result.miss_hit, HIT_STORE);


      for( int i=0; i<2; i++ ){    
      status = nru_replacement_policy(idx, 
                                     300, 
                                     associativity,
                                     LOAD,
                                     cache_line_A,
                                     &result,
                                     bool(debug));
      }/* for end */

      EXPECT_EQ(status, OK);
      /* read hit */
      EXPECT_EQ(result.miss_hit, HIT_LOAD);
      

      for( int i=0; i<2; i++ ){
        status = nru_replacement_policy(idx, 
                                     4000, 
                                     associativity,
                                     LOAD,
                                     cache_line_B,
                                     &result,
                                     bool(debug));
      }/* for end */

      EXPECT_EQ(status, OK);
      /* read hit */
      EXPECT_EQ(result.miss_hit, HIT_LOAD);


      for( int i = 0; i<associativity; i++)
      {
        tag++;
        status = nru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     LOAD,
                                     cache_line_B,
                                     &result,
                                     bool(debug));
      }/* for end */

      EXPECT_EQ(status, OK);
      EXPECT_EQ( cache_line_B[idx].dirty, false);


      tag = 300;
     
      /* Insert new cache block until get an eviction in */
      /* the first inserted block                        */    
      for( int i=0; i<associativity; i++)
      {
        tag++;
        nru_replacement_policy(idx, 
                              tag, 
                              associativity,
                              LOAD,
                              cache_line_A,
                              &result,
                              bool(debug));
      }/* for end */

      EXPECT_EQ( result.evicted_address, INIT );

      break;
    

    /* SRRIP */
    case 2:
      for( int i=0; i<2; i++ ){
        status = srrip_replacement_policy(idx, 
                                     4000, 
                                     associativity,
                                     STORE,
                                     cache_line_A,
                                     &result,
                                     bool(debug));
      }/* for end */

      EXPECT_EQ(status, OK);
      /* write hit */
      EXPECT_EQ(result.miss_hit, HIT_STORE);


      for( int i=0; i<2; i++ ){    
      status = srrip_replacement_policy(idx, 
                                     300, 
                                     associativity,
                                     LOAD,
                                     cache_line_A,
                                     &result,
                                     bool(debug));
      }/* for end */

      EXPECT_EQ(status, OK);
      /* read hit */
      EXPECT_EQ(result.miss_hit, HIT_LOAD);
      

      for( int i=0; i<2; i++ ){
        status = srrip_replacement_policy(idx, 
                                     4000, 
                                     associativity,
                                     LOAD,
                                     cache_line_B,
                                     &result,
                                     bool(debug));
      }/* for end */

      EXPECT_EQ(status, OK);
      /* read hit */
      EXPECT_EQ(result.miss_hit, HIT_LOAD);


      for( int i = 0; i<associativity; i++)
      {
        tag++;
        status = srrip_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     LOAD,
                                     cache_line_B,
                                     &result,
                                     bool(debug));
      }/* for end */

      EXPECT_EQ(status, OK);
      EXPECT_EQ( cache_line_B[idx].dirty, false);


      tag = 300;
     
      /* Insert new cache block until get an eviction in */
      /* the first inserted block                        */    
      for( int i=0; i<associativity; i++)
      {
        tag++;
        srrip_replacement_policy(idx, 
                              tag, 
                              associativity,
                              STORE,
                              cache_line_A,
                              &result,
                              bool(debug));

        if( result.dirty_eviction == true ) i = associativity; 
      }/* for end */

      EXPECT_EQ( result.evicted_address, 0 );

      break;

    default:
      return;
      break;
  }/* switch end */

}/* TEST5 end */



/*
 * TEST6: Verifies an error is return when invalid parameters are pass
 * performed.
 * 1. Choose a random policy 
 * 2. Choose invalid parameters for idx, tag and asociativy
 * 3. Check function returns a PARAM error condition
 */
TEST(L1cache, boundaries){
  
  int    status;
  int    idx;
  int    tag;
  int    associativity;
  bool   loadstore                 = (rand()%1 ==0)? false:true ;
  bool   debug                     = 0;
  struct operation_result result   = {};
  int    policy;

  /* Random policy */
  policy = rand()%RRIP;

  struct entry cache_line[associativity];

  /* Fill cache line */
  for ( int i = 0; i < associativity; i++) 
  {
    cache_line[i].valid    = true;
    cache_line[i].tag      = rand()%4090;
    cache_line[i].dirty    = false;
    cache_line[i].rp_value = RRPV_ACTUAL - 1;
  }/* for end */


  DEBUG(debug,Checking PARAM error);

  /* Associativity param error*/
  switch( policy )
  {
    /* LRU */
    case LRU:
      tag = rand()%1000;
      idx = rand()%1000;
      associativity = INIT;

      status = lru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug));
      
      EXPECT_EQ( status, PARAM );

      break;

    /* NRU */
    case NRU:
      tag = rand()%1000;
      idx = rand()%1000;
      associativity = INIT;

      status = nru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug));
      
      EXPECT_EQ( status, PARAM );
      break;

    /* SRRIP */
    case RRIP:
      tag = rand()%1000;
      idx = rand()%1000;
      associativity = INIT;

      status = srrip_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug));
      
      EXPECT_EQ( status, PARAM );
      break;

    default:
      return;
      break;
  }/* switch end */

}/* TEST6 end */