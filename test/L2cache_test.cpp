/*
 *  Advanced Cache simulation project
 *  Class UCR IE-521
 *  Semester: II-2019
 *  Jorge Munoz Taylor
 *  A53863
*/

#include <gtest/gtest.h>
#include <time.h>
#include <stdlib.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <L2cache.h>

#define KB 1024
#define KB4 4096
#define MIN_ASSOCIATIVITY 1
#define MAX_ASSOCIATIVITY 4
#define L2_MULT 4
#define L2_ASSO_MULT 2
#define INIT 0

using namespace std;

class L2cache : public ::testing::Test{
	protected:
		int debug_on;
		virtual void SetUp()
		{
  		/* Parse for debug env variable */
  		get_env_var("TEST_DEBUG", &debug_on);
		};
};

/*
 * TEST3: Verifies L1 hit and L2 hit state. 
 * 1. Choose a random associativity
 * 2. Choose a random tag
 * 2. Fill L1 and L2 cache  
 * 3.
 */
TEST_F(L2cache, l1_hit_l2_hit){

	int status = OK;
	int idx, tag, i;
	int loadstore;
	int associativity, l2_associativity;
	struct l1_l2_entry_info l1_l2_info;
  	struct operation_result l1_result = {};
  	struct operation_result l2_result = {};

	/* Fill a random cache entry */
	idx = rand()% KB;
	tag = rand()%KB4;
	associativity    = MIN_ASSOCIATIVITY << (rand() % MAX_ASSOCIATIVITY);
	l2_associativity = L2_ASSO_MULT*associativity;

	l1_l2_info.l1_idx           = INIT;
	l1_l2_info.l1_tag			= KB4;
	l1_l2_info.l1_associativity = associativity;
	l1_l2_info.l2_idx			= INIT;
	l1_l2_info.l2_tag			= KB4;
	l1_l2_info.l2_associativity	= l2_associativity;

	struct entry l1_cache_blocks[ KB*associativity];
	struct entry l2_cache_blocks[ L2_MULT*KB*l2_associativity];

	/* Check for a hit */
	DEBUG(debug_on, l1_hit_l2_hit_test);

	for (i = 0 ; i < 2; i++)
	{
		/* Fill cache line */
		for ( i =  0; i < associativity; i++)
		{
			l1_cache_blocks [idx*associativity+i].valid = true;
			l1_cache_blocks [idx*associativity+i].tag   = rand()%KB4;
			l1_cache_blocks [idx*associativity+i].dirty = false;

			while ( l1_cache_blocks[idx*associativity+i].tag == tag ) 
			{
        		l1_cache_blocks[idx*associativity+i].tag = rand()%KB4;
      		}
		}/* For end */


		for ( int j=0; j<2; j++)
			status = lru_replacement_policy_l1_l2(&l1_l2_info,
												true,
												l1_cache_blocks,
												l2_cache_blocks,
												&l1_result,
												&l2_result);
		

		EXPECT_EQ( status, OK );

		EXPECT_EQ( l1_result.miss_hit,  HIT_STORE );

		loadstore = ( l1_result.miss_hit == MISS_LOAD ) ? HIT_LOAD : HIT_STORE;
		EXPECT_EQ( l2_result.miss_hit,  loadstore );

	}/* For end */

}/* Test 3 end */



/*
 * TEST4: Verifies L1 miss and L2 hit state. 
 * 1. Choose a random associativity
 * 2. Fill L1 cache entry
 * 3. Force a L1 HIT load or store
 * 4. Check miss_hit_status == MISS_STORE
 * 5. Force a L2 HIT load or store
 * 6. Check miss_hit_status == MISS_STORE
 */
TEST_F(L2cache, l1_miss_l2_hit){

	int status = OK;
	int idx, tag, i;
	bool loadstore;
	int  load_or_store;
	int associativity;
	struct l1_l2_entry_info l1_l2_info;
  	struct operation_result l1_result = {};
  	struct operation_result l2_result = {};

	/* Fill a random cache entry */
	idx = rand()% KB;
	tag = rand()% KB4;
	associativity = MIN_ASSOCIATIVITY << (rand()% MAX_ASSOCIATIVITY);

	l1_l2_info.l1_idx           = idx;
	l1_l2_info.l1_tag			= KB4 + 1;
	l1_l2_info.l1_associativity = associativity;

	l1_l2_info.l2_idx			= idx;
	l1_l2_info.l2_tag			= KB4;
	l1_l2_info.l2_associativity	= associativity;

	struct entry l1_cache_blocks[ KB*associativity];
	struct entry l2_cache_blocks[ KB*associativity];

	/* Check for a hit */
	DEBUG(debug_on, l1_miss_l2_hit_test);

	loadstore = rand() % STORE;

		/* Fill cache line */
		for ( i =  0; i < associativity; i++)
		{
			l2_cache_blocks [idx*associativity+i].valid = true;
			l2_cache_blocks [idx*associativity+i].tag   = KB4;
			l2_cache_blocks [idx*associativity+i].dirty = false;

			while ( l2_cache_blocks[idx*associativity+i].tag == tag ) 
			{
        		l2_cache_blocks[idx*associativity+i].tag = rand()%KB4;
      		}
		}/* For end */


		
		status = lru_replacement_policy_l1_l2(&l1_l2_info,
											loadstore,
											l1_cache_blocks,
											l2_cache_blocks,
											&l1_result,
											&l2_result);
		

		EXPECT_EQ( status, OK );

		load_or_store = (loadstore == LOAD) ? MISS_LOAD : MISS_STORE;
		EXPECT_EQ( l1_result.miss_hit, load_or_store );

		load_or_store = ( l1_result.miss_hit == MISS_LOAD ) ? HIT_LOAD : HIT_STORE;
		EXPECT_EQ( l2_result.miss_hit,  load_or_store );


}/* Test 4 end */



/*
 * TEST5: Verifies L1 miss and L2 miss state. 
 * 1. Choose a random associativity
 * 2. Fill L1 cache entry
 * 3. Force a L1 HIT load or store
 * 4. Check miss_hit_status == MISS_STORE
 * 5. Force a L2 HIT load or store
 * 6. Check miss_hit_status == MISS_STORE
 */
TEST_F(L2cache, l1_miss_l2_miss){

	int status = OK;
	int idx, tag, i;
	bool loadstore;
	int  load_or_store;
	int associativity, l2_associativity;
	struct l1_l2_entry_info l1_l2_info;
  	struct operation_result l1_result = {};
  	struct operation_result l2_result = {};

	/* Fill a random cache entry */
	idx = rand()% KB;
	tag = rand()% KB4;
	associativity    = MIN_ASSOCIATIVITY << (rand()% MAX_ASSOCIATIVITY);
	l2_associativity = 2*associativity;

	l1_l2_info.l1_idx           = INIT;
	l1_l2_info.l1_tag			= KB4 + 1;
	l1_l2_info.l1_associativity = associativity;
	l1_l2_info.l2_idx			= INIT;
	l1_l2_info.l2_tag			= KB4 + 1;
	l1_l2_info.l2_associativity	= l2_associativity;

	struct entry l1_cache_blocks[ KB*associativity];
	struct entry l2_cache_blocks[ L2_MULT* KB*l2_associativity];

	/* Check for a hit */
	DEBUG(debug_on, l1_miss_l2_miss_test);

	loadstore = rand()% STORE;


		/* Fill cache line */
		for ( i =  0; i < associativity; i++)
		{
			l2_cache_blocks [idx*associativity+i].valid    = true;
			l2_cache_blocks [idx*associativity+i].tag      = rand()%KB4;
			l2_cache_blocks [idx*associativity+i].dirty    = false;

			while ( l2_cache_blocks[idx*associativity+i].tag == tag ) 
			{
        		l2_cache_blocks[idx*associativity+i].tag = rand()%KB4;
      		}
		}/* For end */


		
		status = lru_replacement_policy_l1_l2(&l1_l2_info,
											loadstore,
											l1_cache_blocks,
											l2_cache_blocks,
											&l1_result,
											&l2_result);
		

		EXPECT_EQ( status, OK );

		load_or_store = (loadstore == LOAD) ? MISS_LOAD : MISS_STORE;
		EXPECT_EQ( l1_result.miss_hit, load_or_store );

		load_or_store = ( l1_result.miss_hit == MISS_LOAD ) ? MISS_LOAD : MISS_STORE;
		EXPECT_EQ( l2_result.miss_hit,  loadstore );

}/* Test 5 end */



/*
 * TEST6: Invalidation of L1 for victimization in L2 
 * 1. Choose a random associativity
 * 2. Fill L1 cache entry
 * 3. Force a L1 HIT load or store
 * 4. Check miss_hit_status == MISS_STORE
 * 5. Force a L2 HIT load or store
 * 6. Check miss_hit_status == MISS_STORE
 */
TEST_F(L2cache, l1_invalid_l2_victimization){

	int status = OK;
	int idx, tag, i;
	bool loadstore;
	int  load_or_store;
	int associativity, l2_associativity;
	struct l1_l2_entry_info l1_l2_info;
  	struct operation_result l1_result = {};
  	struct operation_result l2_result = {};

	/* Fill a random cache entry */
	idx = rand()% KB;
	tag = rand()% KB4;
	associativity    = MIN_ASSOCIATIVITY << (rand()% MAX_ASSOCIATIVITY);
	l2_associativity = 2*associativity;

	l1_l2_info.l1_idx           = INIT;
	l1_l2_info.l1_tag			= KB4 + 1;
	l1_l2_info.l1_associativity = associativity;
	l1_l2_info.l2_idx			= INIT;
	l1_l2_info.l2_tag			= KB4 + 1;
	l1_l2_info.l2_associativity	= l2_associativity;

	struct entry l1_cache_blocks[ KB*associativity];
	struct entry l2_cache_blocks[ L2_MULT* KB*l2_associativity];

	/* Check for a hit */
	DEBUG(debug_on, l1_invalid_l2_victimization_test);

	loadstore = rand()% STORE;


		/* Fill cache line */
		for ( i =  0; i < associativity; i++)
		{
			l2_cache_blocks [idx*associativity+i].valid = true;
			l2_cache_blocks [idx*associativity+i].tag   = rand()%KB4;
			l2_cache_blocks [idx*associativity+i].dirty = false;

			while ( l2_cache_blocks[idx*associativity+i].tag == tag ) 
			{
        		l2_cache_blocks[idx*associativity+i].tag = rand()%KB4;
      		}
		}/* For end */


		
		status = lru_replacement_policy_l1_l2(&l1_l2_info,
											loadstore,
											l1_cache_blocks,
											l2_cache_blocks,
											&l1_result,
											&l2_result);


		EXPECT_EQ( status, OK );


        if( l2_result.miss_hit == MISS_LOAD || l2_result.miss_hit == MISS_STORE )
        {
          if( l2_result.valid == true )
          {
            status = l1_line_invalid_set(l2_result.evicted_address,
                                l2_result.tag,
                                l1_l2_info.l1_associativity,
                                l1_cache_blocks,
                                false);
          }
        }/* If end */


		EXPECT_EQ( status, OK );

}/* Test 6 end */