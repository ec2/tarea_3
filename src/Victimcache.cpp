/*
 *  Advanced Cache simulation project
 *  Class UCR IE-521
 *  Semester: II-2019
 *  Jorge Munoz Taylor
 *  A53863
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <Victimcache.h>

#define KB 1024
#define ADDRSIZE 32
#define VC_FIRST_LINE 0 /* Tell which line in the victim is the first */

using namespace std;

int lru_replacement_policy_l1_vc(const l1_vc_entry_info *l1_vc_info,
      	                      	bool loadstore,
        	                    entry* l1_cache_blocks,
          	                  	entry* vc_cache_blocks,
            	                operation_result* l1_result,
              	              	operation_result* vc_result,
                	            bool debug)
{
	int status = OK;


	/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	/*                     L1                      */
	/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

	status = lru_replacement_policy (l1_vc_info->l1_idx,
								l1_vc_info->l1_tag,
								l1_vc_info->l1_associativity,
								loadstore,
								l1_cache_blocks,
								l1_result,
								false);
					
	
	if( status != OK ) return ERROR;


	status = write_back (l1_vc_info->l1_idx,
					l1_vc_info->l1_associativity,
					l1_cache_blocks,
					l1_result);


	if( status != OK ) return ERROR;


	/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	/*                    Victim                   */
	/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

	if( l1_result->miss_hit == MISS_LOAD || l1_result->miss_hit == MISS_STORE )
	{
		status = lru_replacement_policy (VC_FIRST_LINE,
										l1_vc_info->l1_tag,
										l1_vc_info->vc_associativity,
										LOAD,
										vc_cache_blocks,
										vc_result,
										false);	


		if( status != OK ) return ERROR;
		

		/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
		/*    The evicted L1 block is stored in the    */
		/*            victim MRU position              */
		/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

		if( vc_result->miss_hit == MISS_LOAD )
		{
			vc_cache_blocks[ VC_FIRST_LINE ].valid = l1_result->valid;
			vc_cache_blocks[ VC_FIRST_LINE ].tag   = l1_result->tag;			
		}/* if end */

		else if( vc_result->miss_hit == HIT_LOAD )
		{
			l1_cache_blocks[ l1_vc_info->l1_idx ].dirty = (loadstore==LOAD) ? false : true; 
		}

		/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
		/*                 Error control               */
		/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
		
		if( vc_result->miss_hit == HIT_STORE || vc_result->miss_hit == MISS_STORE )
		{
			return ERROR;
		}/* Else if end */

	}/* if end */

	return OK;

}/* End of function */