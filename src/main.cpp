/*
 *  Advanced Cache simulation project
 *  Class UCR IE-521
 *  Semester: II-2019
 *  Jorge Munoz Taylor
 *  A53863
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <L1cache.h>
#include <L2cache.h>
#include <Victimcache.h>
#include <debug_utilities.h>

#include <ctype.h> /*Include the function isdigit()*/
#include <ctime>

#define ADDRSIZE 32
#define KB 1024

#define INIT_VALUE 0 /* Indicate the initial value of the var */
#define VC 0 /* Indicate that VC is selected */
#define L2 1 /* Indicate that L2 is selected */

#define L2_size_mul 4 /* L2 size multiplier */
#define L2_aso_mul  2 /* L2 associativity multiplier */
#define VICTIM_SIZE 16 /* Size of the victim cache */

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/*    Tells the user how to run the program    */
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
void print_usage ()
{
  printf("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
  printf("\n The format for run the program is:\n");
  printf("\n   gunzip -c [PATH]/mcf.trace.gz | [PATH]/tarea_3/src/adv_cache -t <#> -a <#> -l <#> -opt <l2/vc>\n\n");
  printf("              or \n\n");
  printf("   gunzip -c [PATH]/art.trace.gz | [PATH]/tarea_3/src/adv_cache -t <#> -a <#> -l <#> -opt <l2/vc>");
  printf("\n\n -> The # is an int number, opt ONLY can be l2 or vc <-\n");
  printf("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
  exit (0);
}/* End of function */

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/*  Print in console the console confguration  */
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
void print_cache_configuration
(
  int _opt,
  int _cache_size,
  int _associativity,
  int _block_size
)
{
  printf("\n____________________________________\n\n");
  printf("  Cache parameters:" );
  printf("\n____________________________________\n\n");
 
  if( _opt == L2 )
  {
    printf("  L1 Cache size (KB):        %d\n", _cache_size/KB);
    printf("  L2 Cache size (KB):        %d\n", L2_size_mul*_cache_size/KB);
    printf("  Cache L1 Associativity:    %d\n", _associativity);
    printf("  Cache L2 Associativity:    %d\n", L2_aso_mul*_associativity);
    printf("  Cache Block Size (bytes):  %d\n", _block_size);
    printf("____________________________________\n");
  }
  else if( _opt == VC )
  {
    printf("  L1 Cache size (KB):        %d\n", _cache_size/KB);
    printf("  Cache L1 Associativity:    %d\n", _associativity);
    printf("  Cache Block Size (bytes):  %d\n", _block_size);
    printf("____________________________________\n");    
  }
  else exit(0);  
  
}; /* End of function */

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/* Print in console the results of the simulation */
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
void print_statistics 
(
  int _opt,
  int _dirty_evictions,
  int _l1_misses,
  int _l1_hits,
  int _l2_misses,
  int _l2_hits,

  int _l1_vc_misses,
  int _l1_vc_hits,
  int _vc_hits
)
{
  float _l1_miss_rate;
  float _l2_miss_rate;
  float _overall_miss_rate;
  float _global_miss_rate;
  float _l1_vc_miss_rate;

  if( _opt == L2 )
  {
    _l1_miss_rate = (float)_l1_misses /( (float)_l1_misses + (float)_l1_hits + (float)_l2_misses  + (float)_l2_hits );
    _l2_miss_rate = (float)_l2_misses /( (float)_l1_misses + (float)_l1_hits + (float)_l2_misses  + (float)_l2_hits );
    
    _global_miss_rate = ( (float)_l1_misses+(float)_l2_misses )/( (float)_l1_misses + (float)_l1_hits + (float)_l2_misses + (float)_l2_hits );
    _overall_miss_rate = (_l1_miss_rate + _l2_miss_rate)/2;

    printf("\n  Simulation results:             \n" );
    printf("____________________________________\n\n"); 
    printf("  Overall miss rate:         %.2f\n", _overall_miss_rate );
    printf("  L1 miss rate:              %.2f\n", _l1_miss_rate );
    printf("  L2 miss rate:              %.2f\n", _l2_miss_rate );
    printf("  Global miss rate:          %.2f\n", _global_miss_rate );
    printf("  Misses (L1):               %d\n"  , _l1_misses );
    printf("  Hits (L1):                 %d\n"  , _l1_hits );
    printf("  Misses (L2):               %d\n"  , _l2_misses );
    printf("  Hits (L2):                 %d\n"  , _l2_hits );
    printf("  Dirty evictions:           %d\n"  , _dirty_evictions );
    printf("____________________________________\n");
  }
  else if( _opt == VC )
  {
    _l1_vc_miss_rate =  (float)_l1_vc_misses / ( (float)_l1_vc_misses + (float)_l1_vc_hits );

    printf("  Miss rate (L1+VC):         %.2f\n", _l1_vc_miss_rate);
    printf("  Misses (L1+VC):            %d\n", _l1_vc_misses );
    printf("  Hits (L1+VC):              %d\n", _l1_vc_hits );
    printf("  Victim cache hits:         %d\n", _vc_hits );
    printf("  Dirty evictions:           %d\n"  , _dirty_evictions ); 
  }
  else exit(0);

};/* End of function */


int main(int argc, char * argv []) {
  
  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  /*               Program vars                  */
  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  
  int          _cache_size;
  int          _associativity;
  int          _block_size;
  uint8_t      _opt;
  bool         _arg_error = (bool)INIT_VALUE;
  
  char         _hastag; 
  int          _ls;
  unsigned int _hex_address;
  long         _bin_address;
  int          _ic;

  int          l1_tag_size       = INIT_VALUE;
  int          l1_idx_size       = INIT_VALUE;
  int          l1_offset_size    = INIT_VALUE;

  int          l2_tag_size       = INIT_VALUE;
  int          l2_idx_size       = INIT_VALUE;
  int          l2_offset_size    = INIT_VALUE;
  
  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

  int          idx; /* address in binary of the SETS*/
  int          tag;
   
  int          _dirty_evictions = INIT_VALUE;
  
  int          _l1_load_miss    = INIT_VALUE;
  int          _l1_store_miss   = INIT_VALUE;
  int          _l1_load_hits    = INIT_VALUE;
  int          _l1_store_hits   = INIT_VALUE;

  int          _l2_load_miss    = INIT_VALUE;
  int          _l2_store_miss   = INIT_VALUE;
  int          _l2_load_hits    = INIT_VALUE;
  int          _l2_store_hits   = INIT_VALUE;

  int          _vc_load_miss    = INIT_VALUE;
  int          _vc_load_hits    = INIT_VALUE;

  int          _l1_vc_misses    = INIT_VALUE;
  int          _l1_vc_hits      = INIT_VALUE;
  int          _vc_hits         = INIT_VALUE;


  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  /*             Structs declaration             */ 
  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

  struct timespec t0, t1;
  struct operation_result l1_operation_res = {};
  struct operation_result l2_operation_res = {};
  struct operation_result vc_operation_res = {};
  struct l1_l2_entry_info l1_l2_info       = {};
  struct l1_vc_entry_info l1_vc_info       = {};


  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  /*                Parse arguments              */
  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  
  if(argc == 9) /*User NEED to pass 9 arguments*/
  {
    if( strcmp("-t", argv[1]) == 0 )
    {
      if( isdigit( *argv[2] ) != 0 ) _cache_size = 1024*atoi(argv[2]); /* In KB */
      else _arg_error = true;
    }
    else _arg_error = true;
  

    if(strcmp( "-a", argv[3]) == 0 )
    {
      if( isdigit( *argv[4] ) != 0 ) 
      {
        _associativity = atoi(argv[4]);

        if(_associativity == 0)
        {
          printf("\n-> Associativity cant be 0\n\n");
          return _arg_error = true;
        }
      }
      else _arg_error = true;
    }
    else _arg_error = true;
     

    if( strcmp("-l", argv[5]) == 0 )
    {
      if( isdigit( *argv[6]) != 0 ) _block_size = atoi(argv[6]);
      else _arg_error = true;
    }
    else _arg_error = true;
    
 
    if( strcmp("-opt", argv[7]) == 0 )
    {
      if     ( strcmp( "vc", argv[8] ) == 0 ) _opt = VC;
      else if( strcmp( "l2", argv[8] ) == 0 ) _opt = L2;
      else _arg_error = true;
    } 
    else _arg_error = true;

  }
  /*User pass wrong number of arguments*/
  else _arg_error = true;

  /* If the user pass wrong args print a program usage */
  if(_arg_error == true) print_usage();
  

  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  /*           Define L1 cache memory            */
  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

  entry* L1_CACHE_BLOCK = new entry[ _cache_size/_block_size ];
  
  
  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  /*          Initialize the L1 cache            */
  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

  for( int i = 0; i < _cache_size/_block_size; i++)
  {
    L1_CACHE_BLOCK[i].valid = INIT_VALUE;
    L1_CACHE_BLOCK[i].dirty = false;
  }/* for end */

  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  /*  Obtain the tag size, idx size and offset   */ 
  /*           size in bits of the L1            */
  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

  field_size_get(
    _cache_size,
    _associativity,
    _block_size, 
    &l1_tag_size,
    &l1_idx_size,
    &l1_offset_size
  );

  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  /*      Print in terminal the actual cache     */
  /*              configuration                  */
  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

  print_cache_configuration(
    _opt,
    _cache_size,
    _associativity,
    _block_size
  );


  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  /*               Program start                 */
  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  /*             multilevel cache                */
  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  
  if( _opt == L2 )
  {
    /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    /*           Define L2 cache memory            */
    /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

    entry* L2_CACHE_BLOCK = new entry[ (L2_size_mul*_cache_size)/_block_size ];

    
    /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    /*          Initialize the L2 cache            */
    /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

    for( int i = 0; i < L2_size_mul*(_cache_size/_block_size); i++)
    {
      L2_CACHE_BLOCK[i].valid = INIT_VALUE;
      L2_CACHE_BLOCK[i].dirty = false;
    }/* for end */


    /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    /*  Obtain the tag size, idx size and offset   */ 
    /*           size in bits of the L2            */
    /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/ 
  
    field_size_get(
      L2_size_mul*_cache_size,
      L2_aso_mul*_associativity,
      _block_size, 
      &l2_tag_size,
      &l2_idx_size,
      &l2_offset_size
    );


    /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    /*     Assign associativity for L1 and L2      */
    /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

    l1_l2_info.l1_associativity = _associativity;
    l1_l2_info.l2_associativity = L2_aso_mul*_associativity;


    /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    /* Init the time counter and store the value   */
    /*              in the var t0                  */
    /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

    clock_gettime( CLOCK_MONOTONIC, &t0 );
   

    /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    /*              Init main while                */
    /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

    while( scanf("%c %d %x %d\n", &_hastag, &_ls, &_hex_address, &_ic) != EOF )
    {
        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
        /*  Convert the unsigned int address to long   */
        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

        _bin_address = _hex_address | 0;


        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
        /*    Obtain the tag and idx for search the    */ 
        /*        line in the L1 cache blocks          */
        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

        address_tag_idx_get( 
          _bin_address, 
          l1_tag_size, 
          l1_idx_size, 
          l1_offset_size, 
          idx, 
          tag);

        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
        /*      Assign the idx and tag for L1          */
        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

        l1_l2_info.l1_idx = idx;
        l1_l2_info.l1_tag = tag;


        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
        /*    Obtain the tag and idx for search the    */ 
        /*        line in the L2 cache blocks          */
        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

        address_tag_idx_get( 
          _bin_address, 
          l2_tag_size, 
          l2_idx_size, 
          l2_offset_size, 
          idx, 
          tag);


        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
        /*      Assign the idx and tag for L2          */
        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

        l1_l2_info.l2_idx = idx;
        l1_l2_info.l2_tag = tag;


        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
        /*     Call the function that contain the      */
        /*          multilevel cache algorithm         */
        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

        lru_replacement_policy_l1_l2(
            &l1_l2_info,
						(bool)_ls,
						L1_CACHE_BLOCK,
						L2_CACHE_BLOCK,
						&l1_operation_res,
						&l2_operation_res,
						false); 


        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
        /*    Read the l1_operation_res struct and     */
        /*   determine if there is a load or store     */
        /*                 miss/hit                    */
        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

        if      ( l1_operation_res.miss_hit == MISS_LOAD  ) _l1_load_miss++;
        else if ( l1_operation_res.miss_hit == MISS_STORE ) _l1_store_miss++;
        else if ( l1_operation_res.miss_hit == HIT_LOAD   ) _l1_load_hits++;
        else if ( l1_operation_res.miss_hit == HIT_STORE  ) _l1_store_hits++;
        else return ERROR;

        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
        /*     Read the l2_operation_res struct and    */
        /*     determine if there is a load or store   */
        /*                    miss/hit                 */
        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

        if      ( l2_operation_res.miss_hit == MISS_LOAD  ) _l2_load_miss++;
        else if ( l2_operation_res.miss_hit == MISS_STORE ) _l2_store_miss++;
        else if ( l2_operation_res.miss_hit == HIT_LOAD   ) _l2_load_hits++;
        else if ( l2_operation_res.miss_hit == HIT_STORE  ) _l2_store_hits++;
        else return ERROR;


        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
        /*   Determine if there is an dirty eviction   */
        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

        if ( l2_operation_res.dirty_eviction == true ) _dirty_evictions++;


        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
        /* Verifies if the evicted L2 line is present  */
        /*  in L1, if this is the case the L1 line is  */
        /*                 invalidated                 */
        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

        if( l2_operation_res.miss_hit == MISS_LOAD || l2_operation_res.miss_hit == MISS_STORE )
        {
          if( l2_operation_res.valid == true )
          {
            l1_line_invalid_set(l2_operation_res.evicted_address,
                                l2_operation_res.tag,
                                l1_l2_info.l1_associativity,
                                L1_CACHE_BLOCK,
                                false);
          }
        }/* If end */
    
    }/* While end */


    /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    /*  End the time counter and store the value   */
    /*              in the var t1                  */
    /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

    clock_gettime( CLOCK_MONOTONIC, &t1 );
  }/* if end */




  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  /*                Victim cache                 */
  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

  else if( _opt == VC )
  {
    /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    /*         Define victim cache memory          */
    /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

    entry* VICTIM_CACHE_BLOCK = new entry[ VICTIM_SIZE ];


    /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    /*        Initialize the victim cache          */
    /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

    for( int i = 0; i < VICTIM_SIZE; i++)
    {
      VICTIM_CACHE_BLOCK[i].valid = INIT_VALUE;
    }/* for end */


    /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    /*     Assign associativity for L1 and VC      */
    /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

    l1_vc_info.l1_associativity = _associativity;
    l1_vc_info.vc_associativity = VICTIM_SIZE;

    /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    /* Init the time counter and store the value   */
    /*              in the var t0                  */
    /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

    clock_gettime( CLOCK_MONOTONIC, &t0 );
   

    /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    /*              Init main while                */
    /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
   
    while( scanf("%c %d %x %d\n", &_hastag, &_ls, &_hex_address, &_ic) != EOF )
    {
        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
        /*  Convert the unsigned int address to long   */
        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
        
        _bin_address = _hex_address | 0;
        

        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
        /*    Obtain the tag and idx for search the    */ 
        /*        line in the L1 cache blocks          */
        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

        address_tag_idx_get( 
          _bin_address, 
          l1_tag_size, 
          l1_idx_size, 
          l1_offset_size, 
          idx, 
          tag);


        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
        /*      Assign the idx and tag for L1          */
        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

        l1_vc_info.l1_idx = idx;
        l1_vc_info.l1_tag = tag;


        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
        /*     Call the function that contain the      */
        /*            victim cache algorithm           */
        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

        lru_replacement_policy_l1_vc( 
          &l1_vc_info,
      	  _ls,
        	L1_CACHE_BLOCK,
          VICTIM_CACHE_BLOCK,
          &l1_operation_res,
          &vc_operation_res,
          false);


        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
        /*    Read the l1_operation_res struct and     */
        /*   determine if there is a load or store     */
        /*                 miss/hit                    */
        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

        if      ( l1_operation_res.miss_hit == MISS_LOAD  ) _l1_load_miss++;
        else if ( l1_operation_res.miss_hit == MISS_STORE ) _l1_store_miss++;
        else if ( l1_operation_res.miss_hit == HIT_LOAD   ) _l1_load_hits++;
        else if ( l1_operation_res.miss_hit == HIT_STORE  ) _l1_store_hits++;
        else return ERROR;

        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
        /*    When there is a HIT in L1 the victim     */
        /*             dont do anything                */
        /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

        if( l1_operation_res.miss_hit == MISS_LOAD || l1_operation_res.miss_hit == MISS_STORE )
        {
          /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
          /*     Read the l2_operation_res struct and    */
          /*     determine if there is a load or store   */
          /*                    miss/hit                 */
          /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

          if      ( vc_operation_res.miss_hit == MISS_LOAD  ) _vc_load_miss++;
          else if ( vc_operation_res.miss_hit == HIT_LOAD   ) _vc_load_hits++;
        }

        /* Determine if there is an dirty eviction */
        if ( l1_operation_res.dirty_eviction == true ) _dirty_evictions++;
    }/* While end */


    /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    /*  End the time counter and store the value   */
    /*              in the var t1                  */
    /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

    clock_gettime( CLOCK_MONOTONIC, &t1 );
  }


  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  /*       The optimization not exist            */
  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  else
  {
    printf( "ERROR\n" );
    exit(0);
  }
      

  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  /*           Print cache statistics            */
  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
 
  print_statistics(
    _opt,
    _dirty_evictions,
    _l1_load_miss + _l1_store_miss,
    _l1_load_hits + _l1_store_hits,
    _l2_load_miss + _l2_store_miss,
    _l2_load_hits + _l2_store_hits,

    _l1_load_miss + _l1_store_miss + _vc_load_miss,
    _l1_load_hits + _l1_store_hits + _vc_load_hits,
    _vc_load_hits
  );
  

  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  /*  Elapsed time for execute the algorithms    */
  /*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  
  double time = (t1.tv_sec - t0.tv_sec);
  time += (t1.tv_nsec - t0.tv_nsec)/1000000000.0;
  printf( "\nExecution time: %f \n", time );
       
  return OK;

}/* end of main */