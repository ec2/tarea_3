/*
 *  Advanced Cache simulation project
 *  Class UCR IE-521
 *  Semester: II-2019
 *  Jorge Munoz Taylor
 *  A53863
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <L2cache.h>

#define KB       1024
#define ADDRSIZE 32
using namespace std;

int lru_replacement_policy_l1_l2(const l1_l2_entry_info *l1_l2_info,
								bool loadstore,
								entry* l1_cache_blocks,
								entry* l2_cache_blocks,
								operation_result* l1_result,
								operation_result* l2_result,
								bool debug) 
{
	int status = OK;

	/* L1 cache */
	status = lru_replacement_policy(l1_l2_info->l1_idx,
								l1_l2_info->l1_tag,
								l1_l2_info->l1_associativity,
								loadstore,
								l1_cache_blocks,
								l1_result,
								false);

	if( status != OK ) return ERROR;


	/* L2 cache */
	if( l1_result->miss_hit != HIT_LOAD )
	{
		/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
		/*        Search or store data in L2           */
		/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
		status = lru_replacement_policy (l1_l2_info->l2_idx,
									l1_l2_info->l2_tag,
									l1_l2_info->l2_associativity,
									(l1_result->miss_hit==MISS_LOAD)? (bool)LOAD : (bool)STORE,
									l2_cache_blocks,
									l2_result,
									false);
		

		if( status != OK ) return ERROR;


		status = write_back(l1_l2_info->l2_idx,
						l1_l2_info->l2_associativity,
						l2_cache_blocks,
						l2_result);


		if( status != OK ) return ERROR;

	}/* If end */

	return OK;
}/* End of function */